<div id="wrapper">


    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content">
                <div class="row">
                    
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Selected Services</h5>
                                <div class="ibox-tools">
                                    <span class="label label-warning-light pull-right"><?= count($selected_service)?> Service</span>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <div>
                                    <div class="feed-activity-list">
                                        <?php foreach($selected_service as $service){ ?>
                                            <div class="feed-element">

                                                <div class="media-body ">
                                                    <?php
                                                        $data_feed=$this->service_model->get_selected_service_by_ids($service['id']);
                                                        $timestamp = strtotime($data_feed['created_at']);
                                                        $strTime = array("second", "minute", "hour", "day", "month", "year");
                                                        $length = array("60","60","24","30","12","10");
                                                        $currentTime = time();
                                                        if($currentTime >= $timestamp) {
                                                            $diff  = time()- $timestamp;
                                                            for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
                                                               $diff = $diff / $length[$i];
                                                            }
                                                            $diff = round($diff);
                                                            $time=$diff . " " . $strTime[$i] . "(s) ago ";
                                                        }else{
                                                            $time= "0 minutes ago";
                                                        }
                                                    ?>
                                                    <small class="pull-right"><?= $time?></small>
                                                    <strong><?= $service['customer_first_name']?></strong>  
                                                    <span><?= $service['name']; ?></span>
                                                    <br>
                                                    <small class="text-muted">
                                                        <?php 
                                                            if($service['service_status']==1){
                                                                echo "Pending";
                                                            }else if($service['service_status']==2){
                                                                echo "Confirmed";
                                                            }else if($service['service_status']==3){
                                                                echo "Cancelled";
                                                            }else if($service['service_status']==4){
                                                                echo "In progress";
                                                            }else{
                                                                echo "Completed";
                                                            } 
                                                        ?>
                                                    </small>

                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <?php 
                                        if($this->session->userdata('type')==1){
                                            $href=base_url().'admin/customers_selected_services';
                                            $name='More Selected Services';
                                        }else{
                                            $href=base_url().'service/service_booking';
                                            $name='More Services';
                                        }
                                    ?>
                                    
                                </div>
                            </div>
                            <div class="small-box bg-yellow">
                                <a href="<?= $href?>" class="small-box-footer"><?= $name; ?><i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><?= $this->session->userdata('type')==1?'Customer':''; ?> feedback_list</h5>
                                <div class="ibox-tools">
                                    <span class="label label-warning-light pull-right"><?= count($feedbacks)?> Feedback</span>
                                </div>
                            </div>
                            <div class="ibox-content no-padding">
                                <ul class="list-group">
                                    <?php 
                                     foreach($feedbacks as $feedback){ ?>
                                        <li class="list-group-item">
                                            <p><a class="text-info" href="#">@<?= $feedback['customer_first_name']?></a> <?= $feedback['comment']?></p>
                                            <?php
                                                $data_feed=$this->admin_model->get_feedback_by_id($feedback['feedback_id']);
                                                $timestamp = strtotime($data_feed['created_at']);
                                                $strTime = array("second", "minute", "hour", "day", "month", "year");
                                                $length = array("60","60","24","30","12","10");
                                                $currentTime = time();
                                                if($currentTime >= $timestamp) {
                                                    $diff  = time()- $timestamp;
                                                    for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
                                                       $diff = $diff / $length[$i];
                                                    }
                                                    $diff = round($diff);
                                                    $time=$diff . " " . $strTime[$i] . "(s) ago ";
                                                }else{
                                                    $time= "0 minutes ago";
                                                }
                                            ?>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> <?= $time ?></small>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                            <?php 
                                if($this->session->userdata('type')==1){
                                    $href=base_url().'admin/feedback';
                                }else{
                                    $href=base_url().'customer/feedback';
                                }
                            ?>
                            <div class="small-box bg-yellow">
                                <a href="<?= $href?>" class="small-box-footer">More Feedback <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">

                            <?php if(!empty($customers)){ ?>
                                <div class="ibox-content">
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                          <h3><?= count($customers) ?></h3>

                                          <p>Customer Registrations</p>
                                        </div>
                                        <div class="icon">
                                          <i class="fa fa-user-plus"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="small-box bg-yellow">
                                    <a href="<?= base_url()?>customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            <?php } ?>        
                        </div>
                        
                    </div>
                    


                </div>
            </div>
        </div>
    </div>
            <?php
            if ($error = $this->session->flashdata('login_success')) {
                ?>
                <script>
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            timeOut: 4000
                        };
                        toastr.success('<?php echo $this->session->flashdata('login_success'); ?>', 'Success');

                    }, 1300);
                </script>
                <?php
            }
            ?>