<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>New Employee</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url()?>dashboard">Dashboard</a>
            </li>
            <!-- <li class="active">
                <strong><?= $title?> Home</strong>
            </li> -->
        </ol>
    </div>
    <div class="col-lg-2">
        <a href="<?= base_url()?>home-list" class="btn btn-success" style="margin-bottom: -80px;margin-left: 11px;"><i class="fa fa-plus mr-2"></i> Home List</a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <h5><?= $title?> Home Details</h5> -->
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    <br>
                    <?php  echo form_open_multipart('homepagecontroller/homelist_add',['class'=>'form-horizontal']);?>
                    <!-- <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>HomepageController/homelist_add"> -->
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="title">Title</label>
                                <input class="form-control" type="text"  name="title" onkeypress="return isCharacterKey(event)"
                                    onkeypress="return isNumberKey(event)" value="<?php echo !empty($ans['title'])?$ans['title']:set_value('title'); ?>" required>
                                <div class="error"><?php echo form_error('title'); ?></div>
                            </div>
                        </div>
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="description">Description</label>
                                <textarea name="description" id="my-info" name="description" class="form-control" placeholder="" rows="4" cols="400"  value="<?php echo set_value('description')?>" required/><?php echo set_value('description')?></textarea>
                                <?php echo form_error('description', '<div class="errors">', '</div>');?>
                            </div>
                        </div>

                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="address">Image</label>
                                <input type="file" name="userfile"><br>
                              
                                 
                            </div>
                        </div>
                        <?php if(!empty($ans['id'])){ ?>
                                <input type="hidden" name="id" value="<?= $ans['id']?$ans['id']:''?>">
                        <?php } ?>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function isNumberKey(evt)
     {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
            return false;
            return true;
     }
     function isCharacterKey(evt)
              {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode==32 || (charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
                    return true;
                    return false;
                
              }
</script>
    