<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>New Employee</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url()?>dashboard">Dashboard</a>
            </li>
            <!-- <li class="active">
                <strong><?= $title?> Home</strong>
            </li> -->
        </ol>
    </div>
    <div class="col-lg-2">
        <a href="<?= base_url()?>home-list" class="btn btn-success" style="margin-bottom: -80px;margin-left: 11px;"><i class="fa fa-plus mr-2"></i> Home List</a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <h5><?= $title?> Home Details</h5> -->
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    <br>
                    <?php  echo form_open_multipart('homepagecontroller/home_update',['class'=>'form-horizontal']);?>
                    <!-- <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>HomepageController/homelist_add"> -->
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control" required value="<?php echo $ans[0]->title;?>">
                                <!-- <label for="title">Title</label>
                                <input class="form-control" type="text"  name="title" required value="<?php echo $ans->title;?>"> -->
                                </div>
                            </div>
                      
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                               <!--  <label for="description">Description</label>
                                <textarea name="description" id="my-info" name="description" class="form-control" placeholder="" rows="4" cols="400"  value="<?php echo $ans->description;?>"><?php echo $ans->description;?></textarea> -->
                                <label> Description</label>
                                    <textarea name="description" id="my-info" name="description" class="form-control" placeholder="" rows="4" cols="400" value="<?php echo $ans[0]->description;?>"><?php echo $ans[0]->description;?></textarea>
                                <?php echo form_error('description', '<div class="errors">', '</div>');?>
                            </div>
                        </div>

                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="address">Image</label>
                                <input type="file" name="user_file"><br>
                                 <?php if(!empty($ans['user_file'])){ ?>
                                    <input type="hidden" name="userfile_hidden" value="<?= $ans['user_file']?$ans['user_file']:''?>">
                                    <img src="<?= base_url()?>uploads/<?= $ans[0]['user_file']?>" style="width:150px;height:150px">
                                <?php } ?>
                            </div>
                        </div>
                        <?php if(!empty($ans['id'])){ ?>
                                <input type="hidden" name="id" value="<?= $ans[0]['id']?$ans[0]['id']:''?>">
                        <?php } ?>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

    