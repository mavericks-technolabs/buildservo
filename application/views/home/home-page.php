<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>New Employee</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url()?>dashboard">Dashboard</a>
            </li>
            <!-- <li class="active">
                <strong><?= $title?> Home</strong>
            </li> -->
        </ol>
    </div>
    <div class="col-lg-2">
        <a href="<?= base_url()?>home-list" class="btn btn-success" style="margin-bottom: -80px;margin-left: 11px;"><i class="fa fa-plus mr-2"></i> Home List</a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <!-- <th class="">Id</th> -->
                                    <th>Titles</th>
                                    <th>Description</th>
                                    <th>image</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                              
                            <?php foreach($ans as $value) {
                                /*echo"<per>";
                                print_r($value);
                                 exit();*/
                                ?>
                                        <tr>
                                            <td>
                                                <?=$value['title'];?>
                                            </td>
                                            <td>
                                                <?=$value['description'];?>
                                            </td>
                                            <td>
                                                 <img src="<?php echo base_url();?>uploads/<?=$value['user_file'];?>"height="200px! important;" width="450px">
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url();?>home-edit" class="btn btn-primary" name="book_service">Edit</a>
                                                 <a href="<?php echo base_url();?>delete_homeaddpage" class="btn btn-danger" name="book_service">Delete</a>

                                               
                                                
                                            </td>
                                        </tr>
                                <?php } ?>
                                </tbody>            
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>

</div>
<!-- <div class="modal fade delete-popup" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div>
                    <h3>Reason For Cancellation</h3>
                    
                    <textarea name="reason" class="form-control" id="reason" cols="30" rows="3" required="required"></textarea>
                </div>
                <div class="text-center popup-content">  
                    <h3> By clicking on <span>"YES"</span>, your booking will be cancle. Do you wish to proceed?</h3>
                    <input  type="hidden" name="id_modal" id="id_modal" value="">
                    <br>
                    <button type="button" id="confirm_btn" class="btn btn-success modal-box-button" >Yes</button>
                    <button type="button" class="btn btn-danger modal-box-button" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>  
</div> -->
<!-- <script> 
    $("#selected_service_cancle").on('click',function(){
        var id=$(this).data('id');
        $("#id_modal").val(id);
        $('#deleteConfirmationModal').modal('show');
    });
    
    $("#confirm_btn").on('click',function(){
        var id=$("#id_modal").val();
        var reason=$("#reason").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "service/selected_services_cancle",
            data: { 'id' : id,'reason':reason },
            success: function(result){
                $('#deleteConfirmationModal').modal('hide');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $('.float-e-margins').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  Booking has been cancelled successfully...! <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                $('.alert').fadeIn().delay(3000).fadeOut(function () {
                    $(this).remove();
                });
                setTimeout(function(){ 
                    location.reload();
                }, 3000);
            }
        });
    });
    
</script>    
<?php
    if (($error = $this->session->flashdata('update_success')) || $error = $this->session->flashdata('add_success')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('<?php echo $error; ?>', 'Success');

        }, 1300);
    </script>
<?php
    }elseif ($error = $this->session->flashdata('update_failed') || $error = $this->session->flashdata('add_failed')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.error('<?php echo $error; ?>', 'Error');

        }, 1300);
    </script>
<?php
    }
?> -->
<script type="text/javascript">
var url="<?php echo base_url();?>";
   function delete_homeaddpage(id)
        {
            /*alert('ABC');*/
         swal(
          {
                title: "Are you sure?",
                text: "Are you sure you want to delete?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
          })
         .then((willDelete) => 
          {
                if(willDelete)
                {
                    window.location = url+"delete_homeaddpage/"+id;
                    swal("Your file is deleted!",
                    {
                        icon: "success",
                    });
                }
                else
                {
                    swal("Your file is safe!");
                }
          });
       }

// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}

</script>    
    