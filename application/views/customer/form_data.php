<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit Profile</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url()?>dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $title?> Profile</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $title?> Customer Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    <br>
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-group row m-b-25">
                            <div class="col-md-6">
                                <label for="customer_first_name">First Name</label>
                                <input class="form-control" type="text"  name="customer_first_name" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo !empty($customer_details['customer_first_name'])?$customer_details['customer_first_name']:set_value('customer_first_name'); ?>" required>
                                <div class="error"><?php echo form_error('customer_first_name'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label for="customer_last_name">Last Name</label>
                                <input class="form-control" type="text"  name="customer_last_name" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo !empty($customer_details['customer_last_name'])?$customer_details['customer_last_name']:set_value('customer_last_name'); ?>" required>
                                <div class="error"><?php echo form_error('customer_last_name'); ?></div>
                            </div>
                        </div>

                        <div class="form-group row m-b-25">
                            <div class="col-md-6">
                                <label for="customer_email">Email</label>
                                <input class="form-control" type="email"  name="customer_email" value="<?php echo !empty($customer_details['customer_email'])?$customer_details['customer_email']:set_value('customer_email'); ?>" required="" >
                                <div class="error"><?php echo form_error('customer_email'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label for="customer_mob">Mobile number</label>
                                <input class="form-control" type="text" name="customer_mob"  maxlength="10" pattern="\d{10}" onblur="cheq_number(this);" title="Please enter exactly 10 digits" value="<?php echo !empty($customer_details['customer_mob'])?$customer_details['customer_mob']:set_value('customer_mob'); ?>" required="" >
                                <div class="error"><?php echo form_error('customer_mob'); ?></div>
                            </div>
                        </div>

                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="customer_address">Address</label>
                                <input class="form-control" type="text" name="customer_address"  value="<?php echo !empty($customer_details['customer_address'])?$customer_details['customer_address']:set_value('customer_address'); ?>" required="" >
                                <div class="error"><?php echo form_error('customer_address'); ?></div>
                            </div>

                        </div>
                        
                        <div class="form-group row m-b-25">
                            <div class="col-md-6">
                                <label>City</label>
                                <input type="text" name="customer_city"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo !empty($customer_details['customer_city'])?$customer_details['customer_city']:set_value('customer_city'); ?>" required>
                                <div class="error"><?php echo form_error('customer_city'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label>Pincode</label>
                                <input type="text" name="customer_pincode"  class="form-control input-lg" value="<?php echo !empty($customer_details['customer_pincode'])?$customer_details['customer_pincode']:set_value('customer_pincode'); ?>" required>
                                <div class="error"><?php echo form_error('customer_pincode'); ?></div>
                            </div>
                        </div>
                            

                        <?php if(!empty($customer_details['customer_profile_id'])){ ?>
                                <input type="hidden" name="customer_profile_id" value="<?= $customer_details['customer_profile_id']?$customer_details['customer_profile_id']:''?>">
                        <?php } ?>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
    if (($error = $this->session->flashdata('update_success')) || $error = $this->session->flashdata('add_success')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('<?php echo $error; ?>', 'Success');

        }, 1300);
    </script>
<?php
    }elseif ($error = $this->session->flashdata('update_failed') || $error = $this->session->flashdata('add_failed')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.error('<?php echo $error; ?>', 'Error');

        }, 1300);
    </script>
<?php
    }
?>
<script>
     function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
          return true;
          return false;
        
      }

      function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }

      function contactno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
        function cheq_number()
      {
          var number=$("#number").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
              url: BASE_URL+'EmployeeController/cheq_number',
              type: 'POST',
              data:  { 'number': number},
              dataType:'json',
              success: function(response) 
              {
                if (response == 'Success.')
                {
                  $('#msg_contact').html('<span style="color: green;">'+'Success.'+"</span>");
                }
                else if(response == 'Contact Number Already Exist.')
                {
                  $('#msg_contact').html('<span style="color: red;">'+'Number already Exist.'+"</span>");
                }
                else
                {
                  $('#msg_contact').html('Please enter valid number.');
                }
              }
          });
      }
</script>
    