<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Global Settings</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url()?>dashboard">Dashboard</a>
            </li>
        </ol>
    </div>
    
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Global Details</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    <br>
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin/addglobalsettings">
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="name">Site Name</label>
                                <input class="form-control" type="text"  name="site_name" required value="<?=$globalsettings->site_name?>">
                                <div class="error"><?php echo form_error('site_name'); ?></div>
                            </div>
                        </div>
                        <div class="form-group row m-b-25">
                            <div class="col-md-6">
                                <label for="name">Contact</label>
                                <input class="form-control" type="text"  name="contact" required value="<?=$globalsettings->contact?>">
                                <div class="error"><?php echo form_error('contact'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input class="form-control" type="email"  name="email" value="<?=$globalsettings->email?>" required="" >
                                <div class="error"><?php echo form_error('email_id'); ?></div>
                            </div>
                        </div>
                        <div class="form-group row m-b-25">
                            <div class="col-md-12">
                                <label for="name">Address</label>
                                <input class="form-control" type="text"  name="address" required value="<?=$globalsettings->address?>">
                                <div class="error"><?php echo form_error('address'); ?></div>
                            </div>
                        </div>
                         <div class="form-group row m-b-25">
                            <div class="col-md-6">
                                <label for="name">Monday-to-Friday</label>
                                <input class="form-control" type="text"  name="business_hours" required value="<?=$globalsettings->business_hours?>">
                                <div class="error"><?php echo form_error('business_hours'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label for="email">Saturday</label>
                                <input class="form-control" type="business_saturday"  name="business_saturday" value="<?=$globalsettings->business_saturday?>" required="" >
                                <div class="error"><?php echo form_error('business_saturday'); ?></div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
    if (($error = $this->session->flashdata('update_success'))) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('<?php echo $error; ?>', 'Success');

        }, 1300);
    </script>
<?php
    }elseif ($error = $this->session->flashdata('update_failed')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.error('<?php echo $error; ?>', 'Error');

        }, 1300);
    </script>
<?php
    }
?>

    