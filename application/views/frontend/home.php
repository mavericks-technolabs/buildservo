<div role="main" class="main">
    <div class="slider-container rev_slider_wrapper" style="height: 760px;">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 800, "gridheight": 760}'>
            <ul>
                
                <li data-transition="fade">
                    <img src="<?php echo base_url(); ?>assets/frontend/img/build/cover2.jpg"  
                         alt=""
                         data-bgposition="center center" 
                         data-bgfit="cover" 
                         data-bgrepeat="no-repeat" 
                         class="rev-slidebg">

                    <div class="tp-caption featured-label"
                         data-x="center"
                         data-y="center" data-voffset="-45"
                         data-start="500"
                         style="z-index: 5"
                         data-transform_in="y:[100%];s:500;"
                         data-transform_out="opacity:0;s:500;">WELCOME TO BUILDSERVO</div>

                    <div class="tp-caption bottom-label"
                         data-x="center"
                         data-y="center" data-voffset="5"
                         data-start="1000"
                         data-transform_idle="o:1;"
                         data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;"
                         data-transform_out="opacity:0;s:500;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                         data-splitin="chars" 
                         data-splitout="none" 
                         data-responsive_offset="on"
                         style="font-size: 23px; line-height: 30px; z-index: 5"
                         data-elementdelay="0.05">Labour Contractor & Service Provider</div>

                    <a class="tp-caption btn btn-lg btn-primary btn-slider-action"
                       data-hash
                       data-hash-offset="85"
                       href="<?php echo base_url(); ?>login"
                       data-x="center" data-hoffset="-80"
                       data-y="center" data-voffset="80"
                       data-start="2200"
                       data-whitespace="nowrap"						 
                       data-transform_in="y:[100%];s:500;"
                       data-transform_out="opacity:0;s:500;"
                       style="z-index: 5"
                       data-mask_in="x:0px;y:0px;">Login!</a>

                    <a class="tp-caption btn btn-lg btn-primary btn-slider-action"
                       data-hash
                       data-hash-offset="85"
                       href="<?php echo base_url(); ?>register"
                       data-x="center" data-hoffset="80"
                       data-y="center" data-voffset="80"
                       data-start="2200"
                       data-whitespace="nowrap"						 
                       data-transform_in="y:[100%];s:500;"
                       data-transform_out="opacity:0;s:500;"
                       style="z-index: 5"
                       data-mask_in="x:0px;y:0px;">Sign Up!</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container" style="margin-top: 40px;">
        <div class="row">
            <div class="col-md-12 center">
                <h2 class="mb-xl"><strong>OUR SERVICES</strong></h2>
            </div>
        </div>

        <div class="row">
            <?php
            foreach ($service_list as $key => $value) 
            {
                ?>
                    <div class="col-md-4">
                        <div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
                            <div class="feature-box-icon">
                              
                               <img src="<?php echo base_url();?>uploads/<?=$value->user_file;?>" height="50px! important;" width="50px" class="" alt="" />
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-sm"><?= $value->title;?></h4>
                                <p class="mb-lg"><?php 
                                                $string = strip_tags($value->description);
                                                if (strlen($string) > 100) {
                                                    $stringCut = substr($string, 0, 100);
                                                    $endPoint = strrpos($stringCut, ' ');
                                                    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                                    $string .= '...';
                                                }
                                                echo $string;
                                            ?></p>
                            </div>
                        </div>
                    </div>
                <?php
            }
            ?>
        </div>

            
    </div>
    <section class="section section-tertiary pb-none mb-none">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h2 class="text-light"><strong>Who</strong> We Are</h2>
                    <h5><p class="text-light">Build servo is home to a comprehensive set of Consulting, Design, Engineering, Management and Construction services.

Build servo services are used individually, in combination, or as a complete package on complex and high specification design and/or construction projects, for creating or upgrading industrial, process, research, commercial and public buildings.
</p></h5>
                    <a class="btn btn-secondary mt-lg mb-sm" href="<?php echo base_url(); ?>about-us">View More</a>
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <img class="img-responsive appear-animation" src="<?php echo base_url(); ?>assets/frontend/img/worker2.png" alt="" data-appear-animation="fadeInUp">
                </div>
            </div>
        </div>
    </section>
    <section class="section section-background section-center mt-none" style="background-image: url(<?php echo base_url(); ?>assets/frontend/img/patterns/swirl_pattern.png);">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="heading-dark mt-xl"><strong>OUR CLIENTS</strong></h2>
                    <div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options='{"items": 1, "loop": false}'>
                        <?php foreach($feedback_list as $fl){
                        ?>
                            <div>
                                <div class="col-md-12">
                                    <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                       
                                        <blockquote>
                                            <p><?= $fl['comment']?></p>
                                        </blockquote>
                                        <div class="testimonial-author">
                                            <p><strong><?= $fl['customer_first_name']." ".$fl['customer_last_name']?></strong></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-12 center">
                <h2 class="word-rotator-title mb-sm"><strong><span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
                            <span class="word-rotate-items">
                                <span>PROFESSIONALISM</span>
                                <span>QUALITY</span>
                            </span>
                        </span></strong> MAKING SURE WE ACT AND COMMUNICATE RESPONSIBLY.</h2>
                <p class="lead">We embody the Build Servo and its independence in our everyday behavior and attitude. We are customer-focused and committed to excellence. We are always clear, concise and accurate. We strive to continually improve quality and promote transparency. We respect client confidentiality and individual privacy.</p>
            </div>
        </div>

        <div class="row mt-xl">
            <div class="counters counters-text-dark">
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
                        <i class="fa fa-user"></i>
                        <strong data-to="<?= count($customer_list)?>" data-append="+">0</strong>
                        <label>Happy Clients</label>
                        <p class="text-color-primary mb-xl">They can’t be wrong</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
                        <i class="fa fa-desktop"></i>
                        <strong data-to="<?= count($service_list)?>" data-append="+">0</strong>
                        <label>Services</label>
                        <p class="text-color-primary mb-xl">Many more to come</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
                        <i class="fa fa-ticket"></i>
                        <strong data-to="<?= count($employee_list)?>" data-append="+">0</strong>
                        <label>Employees</label>
                        <p class="text-color-primary mb-xl">Satisfaction guaranteed</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
                        <i class="fa fa-clock-o"></i>
                        <strong data-to="365" data-append="+">0</strong>
                        <label>Working Hours</label>
                        <p class="text-color-primary mb-xl">Available to you at vary low cost</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>