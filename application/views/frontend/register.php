<style type="text/css">
.featured-box.featured-box-primary.align-left.mt-xlg {
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
}
</style>
<div role="main" class="main" style="margin-top: 80px;">

    <section class="page-header">
        <div class="container">
            <div class="row">
              
            </div>
            <div class="row">
               
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="featured-box featured-box-primary align-left mt-xlg">
                    <div class="box-content">
                        <h4 class="heading-primary text-uppercase mb-md">Create An Account</h4>
                        <form  id="frmSignUp" method="post" >
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>First name</label>
                                        <input type="text" name="customer_first_name"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"value="<?php echo set_value('customer_first_name')?>" required>
                                        <div class="error"><?php echo form_error('customer_first_name'); ?></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Last name</label>
                                        <input type="text" name="customer_last_name"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"value="<?php echo set_value('customer_last_name')?>" required>
                                        <div class="error"><?php echo form_error('customer_last_name'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Mobile number</label>
                                        <input type="text" name="customer_mob"  class="form-control input-lg" maxlength="10" pattern="\d{10}" onblur="cheq_number(this);" title="Please enter exactly 10 digits" value="<?php echo set_value('customer_mob')?>" required>
                                        <div class="error"><?php echo form_error('customer_mob'); ?></div>
                                    </div>
                                    <div id="msg_contact" style="color:red"></div>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>E-mail</label>
                                        <input type="email" name="customer_email" onblur="validate(this);"  class="form-control input-lg" value="<?php echo set_value('customer_email')?>" required>
                                         <div id="msg_email" style="color:red"></div>
                                        <div class="error"><?php echo form_error('customer_email'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Address</label>
                                        <input type="text" name="customer_address"  class="form-control input-lg" value="<?php echo set_value('customer_address')?>" required>
                                        <div class="error"><?php echo form_error('customer_address'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <input type="text" name="customer_city"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)"value="<?php echo set_value('customer_city')?>" required>
                                        <div class="error"><?php echo form_error('customer_city'); ?></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Pincode</label>
                                        <input type="text" name="customer_pincode"  class="form-control input-lg" value="<?php echo set_value('customer_pincode')?>" required>
                                        <div class="error"><?php echo form_error('customer_pincode'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Password</label>
                                        <input type="password" name="customer_password" class="form-control input-lg" value="<?php echo set_value('customer_password')?>" required >
                                        <div class="error"><?php echo form_error('customer_password'); ?></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Re-enter Password</label>
                                        <input type="password" name="confirm_password" class="form-control input-lg" value="<?php echo set_value('confirm_password')?>" required>
                                        <div class="error"><?php echo form_error('confirm_password'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Register" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php
if($error = $this->session->flashdata('mobile_failed'))
{
?>
    <script>
        setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('<?php echo $this->session->flashdata('mobile_failed'); ?>', 'Error');

            }, 1300);
    </script>
<?php
}
elseif($error = $this->session->flashdata('email_failed'))
{
?>
    <script>
        setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('<?php echo $this->session->flashdata('email_failed'); ?>', 'Error');

            }, 1300);
    </script>
<?php
}
?>
<script type="text/javascript">
    function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
          return true;
          return false;
        
      }
      function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
              function contactno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
        
  function validate()
      {
      var $result = $("#msg_email");
      var customer_email = $("#customer_email").val();
      $result.text("");

      if (validateEmail(email))
      {
        if ($result) 
        {
          var customer_email=$("#customer_email").val();
          var BASE_URL = "<?php echo base_url();?>";
          $.ajax(
          {
            url: BASE_URL+'logincontroller/cheqmail',
            type: 'POST',
            data:  { 'customer_email': customer_email},
            dataType:'json',
            success: function(response) 
            {
              if (response == 'Success.')
              {
                $('#msg_email').html('<span style="color: green;">'+'Success.'+"</span>");
              }
              else if(response == 'Email Already Exist.')
              {
                $('#msg_email').html('<span style="color: red;">'+'Email already Exist.'+"</span>");
              }
              else
              {
                $('#msg_email').html('Please enter valid email.');
              }
            }
          });
        }
      }
      else
      {
        $result.text(email + " is not valid.");
        $result.css("color", "red");
      }
      return false;
    }
    
</script>