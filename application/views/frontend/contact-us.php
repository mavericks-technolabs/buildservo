<div role="main" class="main" style="margin-top: 80px;">

    <section class="page-header">
        <div class="container">
            <div class="row">
                
            </div>
            <div class="row">
               
            </div>
        </div>
    </section>

   
    <div id="googlemaps" class="google-map">
        <img src="<?php echo base_url(); ?>assets/frontend/img/contact3.jpg" alt="" height="100%" width="100%" />
    </div>

    <div class="container">
        <?php if($message = $this ->session->flashdata('Message')){?>
            <div class="col-md-12 ">
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?=$message ?>
                </div>
            </div>
        <?php }?> 
        <?php if($message = $this ->session->flashdata('Error')){?>
            <div class="col-md-12 ">
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?=$message ?>
                </div>
            </div>
        <?php }?>
        <div class="row">
            <div class="col-md-6">
                
                <h2 class="mb-sm mt-sm"><strong>CONTACT US</strong></h2>
                <form id="contactForm"  method="POST" name="contactform">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Your name *</label>
                                <input type="text" value="<?= !empty($this->session->userdata('customer_name'))?$this->session->userdata('customer_name'):set_value('name')?>" data-msg-required="Please enter your name." class="form-control" name="name" id="name" required>
                                <div class="error"><?php echo form_error('name'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Your Mobile Number *</label>
                                <input type="text" value="<?= !empty($this->session->userdata('customer_mob'))?$this->session->userdata('customer_mob'):set_value('mobile_no')?>" data-msg-required="Please enter your mobile number."  class="form-control" name="mobile_no" id="mobile_no" required>
                                <div class="error"><?php echo form_error('mobile_no'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <label>Your email address *</label>
                                <input type="email" value="<?= !empty($this->session->userdata('customer_email'))?$this->session->userdata('customer_email'):set_value('email_id')?>" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email_id" id="email_id" required>
                                <div class="error"><?php echo form_error('email_id'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Subject *</label>
                                <input type="text" value="<?= set_value('subject')?>" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
                                <div class="error"><?php echo form_error('subject'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Message *</label>
                                <textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message" required></textarea>
                                <div class="error"><?php echo form_error('message'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Send Message" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">

                <h4 class="heading-primary mt-lg">Get in <strong>Touch</strong></h4>
                <p>Send us your enquiry, We will get in touch with you.</p>

                <hr>

                <h4 class="heading-primary">The <strong>Office</strong></h4>
                <ul class="list list-icons list-icons-style-3 mt-xlg">
                    <li><i class="fa fa-map-marker"></i> <strong>Address:</strong><?=$globalsettings->address;?></li>
                    <li><i class="fa fa-phone"></i> <strong>Phone:</strong><?=$globalsettings->contact;?></li>
                    <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com"><?=$globalsettings->email;?></a></li>
                </ul>

                <hr>

                <h4 class="heading-primary">Business <strong>Hours</strong></h4>
                <ul class="list list-icons list-dark mt-xlg">
                    <li><i class="fa fa-clock-o"></i> <strong>Monday-to-friday:</strong><?=$globalsettings->business_hours;?></li>
                    <li><i class="fa fa-clock-o"></i> <strong>Saturday:</strong><?=$globalsettings->business_saturday;?></li>
                    <li><i class="fa fa-clock-o"></i> <strong>Sunday</strong> - Closed</li>
                </ul>

            </div>

        </div>

    </div>

</div>