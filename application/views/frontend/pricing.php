<style>
.plan
{
 height: 500px!important;
    
}
 
</style>
<div role="main" class="main" style="margin-top: 100px;">

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                        <li class="active">Pricing</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Pricing Packages</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-none"><strong>PRICING PACKAGES</strong></h2>

                <hr class="tall">
            </div>
        </div>

        <div class="row">
            <div class="pricing-table">
                <?php 
                    $i=1;
                    foreach ($package_list as $package){ 
                ?>
                    <div class="col-md-3  col-sm-6 aa">
                        <div class="plan <?= $i==2 ?'most-popular':'' ?>">
                            <?php if ($i==2){ ?>
                                <div class="plan-ribbon-wrapper"><div class="plan-ribbon">Popular</div></div>
                            <?php } ?>
                                <h3><?= $package['package_name'];?><span> &#8377;<?= $package['cost'];?></span></h3>
                            <ul>
                                <?php foreach(explode(',',$package['service_id']) as $service){ 
                                        $service=$this->service_model->get_service_by_id($service); 
                                        
                                ?>
                                    <li><strong><?= $service['name']?></strong></li>
                                <?php }?>
                                <li><a class="btn btn-lg btn-primary" href="javascript:void(0)">Purchase</a></li>
                            </ul>
                        </div>
                    </div>
                <?php $i++; } ?>

            </div>

        </div>

    </div>



</div>