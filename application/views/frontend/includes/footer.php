<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="footer-ribbon">
				<span>Get in Touch</span>
			</div>
			<div class="col-md-4">
				<h4>Pages</h4>
				<ul class="list list-icons list-icons-sm">
					<h5><li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url();?>home">Home</a></li></h5>
					<h5><li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url();?>about-us">About Us</a></li></h5>
					<h5><li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url();?>services">Services</a></li></h5>
					<h5><li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url();?>pricing">Pricing</a></li></h5>
					<h5><li><i class="fa fa-caret-right"></i> <a href="<?php echo base_url();?>contact-us">Contact Us</a></li></h5>
				</ul>
			</div>
			<div class="col-md-4">
				<div class="contact-details">
					<h4>Contact Us</h4>
					<ul class="contact">
						<h5><li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> </p></li><h5>
						<h5><li><p><i class="fa fa-phone"></i> <strong>Phone:</strong>  9595370077/9067 9067 20</p></li><h5>
						<h5><li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">buildservo@gmail.com</a></p></li><h5>
					</ul>
				</div>
			</div>
			<div class="col-md-4">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					<li class="social-icons-facebook"><a href="https://www.facebook.com/labourcontractorANDserviceprovider" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
					<li class="social-icons-twitter"><a href="https://twitter.com/Buildservo1" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
					<li class="social-icons-linkedin"><a href="https://www.linkedin.com/company/buildservo" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
					<li class="social-icons-instagram"><a href=" https://www.instagram.com/buildservo/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-1">
					<a href="<?php echo base_url();?>home" class="logo">
						<img alt="BuildServo" class="img-responsive" width="250" height="150" src="<?php echo base_url(); ?>assets/frontend/img/Build_Servo_Logo.png">
					</a>
				</div>
				<div class="col-md-7">
					<h5><p>© Copyright 2018. All Rights Reserved.</p></h5>
				</div>
				<div class="col-md-4">
					<nav id="sub-menu">
						<ul>
							<li><a href="<?php echo base_url();?>faq">FAQ's</a></li>
							<li><a href="<?php echo base_url();?>sitemap">Sitemap</a></li>
							<li><a href="<?php echo base_url();?>contact-us">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>

<!-- Vendor -->
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/common/common.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.stellar/jquery.stellar.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/isotope/jquery.isotope.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/frontend/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
<script src="<?php echo base_url();?>assets/frontend/js/views/view.home.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url();?>assets/frontend/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo base_url();?>assets/frontend/js/theme.init.js"></script>

		

</body>
</html>
