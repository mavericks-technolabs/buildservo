<div role="main" class="main" style="margin-top: 80px;">

				<section class="page-header">
					<div class="container">
						<div class="row">
							
						</div>
						<div class="row">
							
						</div>
					</div>
				</section>

				<div class="container">

					<h2><strong>FREQUENTLY ASKED QUESTIONS</strong></h2>

					

					<hr>

					<div class="row">
						<div class="col-md-12">

							<div class="toggle toggle-primary" data-plugin-toggle>
								<section class="toggle active">
									<label>How do I go about getting a quotation for my work?</label>
									<p>Please either email us, or give us a quick call. We are very willing to help, and happy to provide for any quotations needed.</p>
								</section>

								<section class="toggle">
									<label>What sort of information do I need to provide for my project?</label>
									<p>Any information that involves a breakdown of the work involved, such as working drawings, a schedule of works and any contract particulars you may have. All of these items can aid us in supplying you with a formal quotation.</p>
								</section>

								<section class="toggle">
									<label>Do you work on day work rates?</label>
									<p>Some projects lean more towards a day work rate, purely due to the type of work it is, this is usually items that are unspecified, or largely un-known how long tasks may take. This method could workout cheaper for the client.</p>
								</section>

								<section class="toggle">
									<label> I have architect drawings, what do I do now?</label>
									<p>You can forward us any architectural drawings, and we will usually make a site visit to meet the client, and run through the works. We like to keep communication open as much as possible, and our aim is to deliver exactly what our clients require.</p>
								</section>

								<section class="toggle">
									<label>I would like an extension to our offices. Do you do architectural drawings?</label>
									<p>We are not qualified architects but we can do design and build, we can provide a certain level of architectural drawings in house. But for projects such as, dwelling extensions, where planning permission is needed. We will forward one of our out of house highly skilled architects to you.</p>
								</section>

								<section class="toggle">
									<label>Do you work with listed buildings?</label>
									<p>Yes we can work with any building.</p>
								</section>

								<section class="toggle">
									<label>Our offices need a gentle refurbishment, carpeting and some door maintenance, do you do this?</label>
									<p>Yes, we have a large client base, to which we have provided any and all office refurbishment and fit out works. Offices, schools, universities you name it we are perfectly suited to handle it.</p>
								</section>

								<section class="toggle">
									<label>Are you a main contractor? Or do you work for larger companies on much larger scale projects?</label>
									<p>Build servo is a main contractor company, with the ability to contract manage / project manage any project we are involved with.</p>
								</section>
							</div>

						</div>

					</div>

				</div>

			</div>