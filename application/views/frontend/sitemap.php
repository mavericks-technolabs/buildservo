<div role="main" class="main" style="margin-top: 100px;">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="#">Home</a></li>
									<li class="active">Pages</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h1>Sitemap</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<div class="row">

						<div class="col-md-4">
							<ul class="nav nav-list mb-xl">
								<li><a href="<?php echo base_url();?>home">Home</a></li>
									<li><a href="<?php echo base_url();?>about-us">About Us</a></li>
									<li><a href="<?php echo base_url();?>services">Services</a></li>
									<li><a href="<?php echo base_url();?>pricing">Pricing</a></li>
									<li><a href="<?php echo base_url();?>blogs">Blogs</a></li>
									<li><a href="<?php echo base_url();?>contact-us">Contact Us</a></li>
							</ul>
						</div>

						<div class="col-md-6 col-md-offset-2 hidden-xs">
							<h2 class="mb-lg">Who <strong>We Are</strong></h2>
							<p class="lead">We aim to be the most competitive and the most productive service organization in the India. Our core competencies in inspection, verification, testing and certification are being continuously improved to be best-in-class. They are at the heart of what we are. Our chosen markets will be solely determined by our ability to be the most competitive and to consistently deliver unequalled service to our customers all over the world.</p>
						</div>

					</div>

				</div>

			</div>