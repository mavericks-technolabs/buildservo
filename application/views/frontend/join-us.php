<div role="main" class="main" style="margin-top: 80px;">

    <section class="page-header">
        <div class="container">
            <div class="row">
               
            </div>
            <div class="row">
               
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-2" >
                <div class="featured-box featured-box-primary align-left mt-xlg">
                    <div class="box-content">
                        <h4 class="heading-primary text-uppercase mb-md">Fill Your Details</h4>
                        <form  id="frmJoinUs" method="post" action="<?php echo base_url();?>addjoinus">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>First name</label>
                                        <input type="text" name="first_name"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo set_value('first_name')?>" required>
                                        <div class="error"><?php echo form_error('first_name'); ?></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Last name</label>
                                        <input type="text" name="last_name"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo set_value('last_name')?>" required>
                                        <div class="error"><?php echo form_error('last_name'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Mobile number</label>
                                        <input type="text" name="mobile_number"  class="form-control input-lg" maxlength="10" pattern="\d{10}" onblur="cheq_number(this);" title="Please enter exactly 10 digits"value="<?php echo set_value('mobile_number')?>" required>
                                        <div class="error"><?php echo form_error('mobile_number'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Skill</label>
                                        <select name="skill_id" id="skill_id" class="form-control">
                                            <option value="">Select Skill</option>
                                            <?php
                                            foreach ($service_list as $key => $value) 
                                            {
                                                ?>
                                                    <option value="<?=$value->name?>"><?=$value->name?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Address</label>
                                        <input type="text" name="address"  class="form-control input-lg" value="<?php echo set_value('address')?>" required>
                                        <div class="error"><?php echo form_error('address'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <input type="text" name="city"  class="form-control input-lg" onkeypress="return isCharacterKey(event)"  onkeypress="return isNumberKey(event)" value="<?php echo set_value('city')?>" required>
                                        <div class="error"><?php echo form_error('city'); ?></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Pincode</label>
                                        <input type="text" name="pincode"  class="form-control input-lg" value="<?php echo set_value('pincode')?>" required>
                                        <div class="error"><?php echo form_error('pincode'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<?php
if($error = $this->session->flashdata('success'))
{
?>
    <script>
        setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('<?php echo $this->session->flashdata('success'); ?>', 'Success');

            }, 1300);
    </script>
<?php
}
elseif($error = $this->session->flashdata('failed'))
{
?>
    <script>
        setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.error('<?php echo $this->session->flashdata('failed'); ?>', 'Error');

            }, 1300);
    </script>
<?php
}
?>
<script type="text/javascript">
    function isCharacterKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode>64 && charCode< 90) || (charCode>96 && charCode< 123))
          return true;
          return false;
        
      }
      function isNumberKey(evt)
             {
                 var charCode = (evt.which) ? evt.which : event.keyCode
                 if (!(charCode > 31 && (charCode < 48 || charCode > 57) || charCode==64))
                    return false;
                    return true;
             }
             function contactno(){          
            $('#phone').keypress(function(e) {
                var a = [];
                var k = e.which;

                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(a.indexOf(k)>=0))
                    e.preventDefault();
            });
        }
        
</script>