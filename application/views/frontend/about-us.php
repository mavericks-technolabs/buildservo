
<div role="main" class="main" style="margin-top: 80px;">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<!-- <div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="<?php echo base_url();?>home">Home</a></li>
						<li class="active">About Us</li>
					</ul>
				</div> -->
			</div>
			<div class="row">
				<!-- <div class="col-md-12">
					<h1>About Us</h1>
				</div> -->
			</div>
		</div>
	</section>
	<div id="googlemaps" class="google-map">
        <img src="<?php echo base_url(); ?>assets/frontend/img/about-head.jpg" alt="" height="100%" width="100%" />
    </div>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<h2 class="word-rotator-title"><i>
					Making Sure We Establish Safe & Healthy  Workplaces.</i>
					
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10">
				<p class="lead">
					We fully protect all Build Servo employees, contractors, visitors, stakeholders, physical assets and the environment from any work-related incident, exposure, and any kind of damage.

.
				</p>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url();?>joinus" class="btn btn-lg btn-primary" style="margin-top: 27px;">Join Our Team!</a>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">
			</div>
		</div>

		<div class="row">
			<div class="col-md-8">
				<h3 class="heading-primary"><strong>WHO WE ARE</strong></h3>
				<p style="font-size:17px">BuildServo is home to a comprehensive set of Consulting, Design, Engineering, Management and Construction services.</p>

				<p style="font-size:17px";>BuildServo services are used individually, in combination, or as a complete package on complex and high specification design and/or construction projects, for creating or upgrading industrial, process, research, commercial and public buildings.</p>

				<p style="font-size:17px";>Our in-house technical, design, engineering, procurement, project management and construction teams have the knowledge and experience required to successfully undertake your project from concept to completion.
				We design and construct state of the art facilities that meet and exceed your expectations.</p>

			</div>
			<div class="col-md-4">
				<div class="featured-box featured-box-primary">
					<div class="box-content">
						<h4 class="text-uppercase">Project Gallery</h4>
						<ul class="thumbnail-gallery" data-plugin-lightbox data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
							<li>
								<a title="Benefits 1" href="<?php echo base_url();?>assets/frontend/img/build/about1.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about11.jpg" alt="">
									</span>
								</a>
							</li>
							<li>
								<a title="Benefits 2" href="<?php echo base_url();?>assets/frontend/img/build/about2.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about22.jpg" alt="">
									</span>
								</a>
							</li>
							<li>
								<a title="Benefits 3" href="<?php echo base_url();?>assets/frontend/img/build/about3.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about33.jpg" alt="">
									</span>
								</a>
							</li>
							<li>
								<a title="Benefits 4" href="<?php echo base_url();?>assets/frontend/img/build/about4.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about44.jpg" alt="">
									</span>
								</a>
							</li>
							<li>
								<a title="Benefits 5" href="<?php echo base_url();?>assets/frontend/img/build/about5.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about55.jpg" alt="">
									</span>
								</a>
							</li>
							<li>
								<a title="Benefits 6" href="<?php echo base_url();?>assets/frontend/img/build/about6.jpg">
									<span class="thumbnail mb-none">
										<img src="<?php echo base_url();?>assets/frontend/img/build/about66.jpg" alt="">
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">
			</div>
		</div>

		<div class="row">
			<div class="container">
				<div class="col-md-12">
					<h2 class="word-rotator-title">
					<center><h4 class="heading-primary"><strong>Our Four Principles.</strong></h4></center>
				</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="col-md-12">
					<h3>Integrity:-</h3>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
                    <p style="font-size: 16px;">MAKING SURE WE BUILD TRUST - We act with integrity and behave responsibly. We abide by the rules, laws and regulations of the countries we are operating in. We speak up; we are confident enough to raise concerns and smart enough to consider any that are brought to us.</p>
                </div>
				</div>
			</div>
		</div>
	</div><br>
	<div class="row">
			<div class="container">
				<div class="col-md-12">
					<h3>Safety & Health:-</h3>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
                    <p style="font-size: 16px;">MAKING SURE WE ESTABLISH SAFE AND HEALTHY WORKPLACES - We fully protect all Build Servo employees, contractors, visitors, stakeholders, physical assets and the environment from any work-related incident, exposure, and any kind of damage.</p>
                </div>
				</div>
			</div>
		</div>
	</div><br>
	<div class="row">
			<div class="container">
				<div class="col-md-12">
					<h3>Sustainability:-</h3>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
                    <p style="font-size: 16px;">MAKING SURE WE ESTABLISH MAKING SURE WE ADD LONG-TERM VALUE TO SOCIETY- We use our scale and expertise to enable a more sustainable future. We ensure that we minimize our impact on the environment throughout the value chain. We are good corporate citizens and invest in the communities in which we operate.</p>
                </div>
				</div>
			</div>
		</div>
	</div><br>
	<div class="row">
			<div class="container">
				<div class="col-md-12">
					<h3>Professionalism & Quality:-</h3>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
                    <p style="font-size: 16px;">MAKING SURE WE ESTABLISH MAKING SURE WE ACT AND COMMUNICATE RESPONSIBLY-
					We embody the Build Servo and its independence in our everyday behavior and attitude. We are customer-focused and committed to excellence. We are always clear, concise and accurate. We strive to continually improve quality and promote transparency. We respect client confidentiality and individual privacy.</p>
                </div>
				</div>
			</div>
		</div>
	</div>

</div>
</div>