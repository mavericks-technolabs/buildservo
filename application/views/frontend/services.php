<div role="main" class="main" style="margin-top: 80px;">

    <section class="page-header">
        <div class="container">
            <div class="row">
              
            </div>
            <div class="row">
          
            </div>
        </div>
    </section>

    <div class="container">

        <h2>Our <strong>Services</strong></h2>

        <div class="row">
            <div class="col-md-10">
                <p class="lead">
                    <span class="alternative-font">MAKING SURE WE BUILD TRUST</span> - We act with integrity and behave responsibly. We abide by the rules, laws and regulations of the countries we are operating in. We speak up; we are confident enough to raise concerns and smart enough to consider any that are brought to us.
                </p>
            </div>
            <div class="col-md-2">
                <a href="<?php echo base_url(); ?>contact-us" class="btn btn-lg btn-primary mt-xl pull-right">Contact Us!</a>
            </div>
        </div>

        <hr>

        <div class="featured-boxes">
            <div class="row">
                <?php
                if (!empty($service_list)) 
                {
                    foreach ($service_list as $key => $value) 
                    {
                        ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="featured-box featured-box-primary featured-box-effect-1 mt-xlg">
                                    <div class="box-content">
                                      <img src="<?php echo base_url();?>assets/images/service/<?=$value->service_image;?>" height="150px! important;" width="150px" class="img-circle"/>
                                        <h4 class="text-uppercase"><?= $value->name?></h4>
                                        <p>
                                            <?php 
                                                $string = strip_tags($value->description);
                                                if (strlen($string) > 50) {
                                                    $stringCut = substr($string, 0, 50);
                                                    $endPoint = strrpos($stringCut, ' ');
                                                    $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                                    $string .= '...';
                                                }
                                                echo $string;
                                            ?>
                                        </p>
                                        <p><a href="<?php echo base_url(); ?>service-details?id=<?= $value->id;?>" class="lnk-primary learn-more">Learn More <i class="fa fa-angle-right"></i></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                } 
                ?>
                
                
            </div>
        </div>

        <hr>
        <div class="container">

            <div class="row center">
                <div class="col-md-12">
                    <h1 class="mb-sm word-rotator-title">
                        Our
                        <strong class="inverted">
                            <span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
                                <span class="word-rotate-items">
                                    <span>Mission</span>
                                    <span>Vision</span>
                                </span>
                            </span>
                        </strong>
                    </h1>
                    <p class="lead">
                        Our mission at build servo is to provide our customer a quality service while keeping project and task on time and in a budget.We handle every project with professionalism and integrity, we always a step forward to ensure that each customer gets a quality product at fair price.

                    </p>
                </div>
            </div>

        </div>

        <div class="home-concept">
            <div class="container">

                <div class="row center">
                    <span class="sun"></span>
                    <span class="cloud"></span>
                    <div class="col-md-2 col-md-offset-1">
                        <div class="process-image appear-animation" data-appear-animation="bounceIn">
                            <img src="<?php echo base_url(); ?>assets/frontend/img/build/strategy.jpg" alt="" />
                            <strong>Strategy</strong>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="200">
                            <img src="<?php echo base_url(); ?>assets/frontend/img/build/planing.jpg" alt="" />
                            <strong>Planning</strong>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="400">
                            <img src="<?php echo base_url(); ?>assets/frontend/img/build/build.jpg" alt="" />
                            <strong>Build</strong>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="project-image">
                            <div id="fcSlideshow" class="fc-slideshow">
                                <ul class="fc-slides">
                                    <li><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/build/work1.jpg" alt="" /></a></li>
                                    <li><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/build/work2.jpg" alt="" /></a></li>
                                    <li>><img class="img-responsive" src="<?php echo base_url(); ?>assets/frontend/img/build/work3.jpg" alt="" /></a></li>
                                </ul>
                            </div>
                            <strong class="our-work">Our Work</strong>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>