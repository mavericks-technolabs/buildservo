<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Joining Enquiries</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Dashboard</a>
            </li>
            <li class="active">
                <strong>Joining Enquiries</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-3">

        
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Joining Enquiries</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
             
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Phone Number</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Pincode</th>
                                    <th>Skill</th>
                                    <th>Date of Joining</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if (!empty($joinus)) {
                                    foreach ($joinus as $key => $value) {
                                        ?>
                                        <tr class="gradeX" id="join-<?= $value['id'] ?>">
                                            <td><?= $value['first_name']." ".$value['last_name']; ?></td>
                                            <td><?= $value['mobile_number']; ?></td>
                                            <td class="center"><?= $value['address']; ?></td>
                                            <td><?= $value['city']; ?></td>
                                            <td><?= $value['pincode']; ?></td>
                                            <td class="center">
                                                <?= $value['skill_id']; ?>
                                            </td>
                                            <td class="center"><?= $value['created_at']; ?></td>
                                            <td>
                                                
                                                <a href="<?= base_url()?>join/delete?id=<?= $value['id'] ?>" class="btn btn-danger delete-join" data-index="<?= $value['id'] ?>" name="delete-join">Delete</a><br>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                            </tbody>            
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<?php
    if (($error = $this->session->flashdata('update_success')) || $error = $this->session->flashdata('add_success')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('<?php echo $error; ?>', 'Success');

        }, 1300);
    </script>
<?php
    }elseif ($error = $this->session->flashdata('update_failed') || $error = $this->session->flashdata('add_failed')) {
?>
    <script>
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.error('<?php echo $error; ?>', 'Error');

        }, 1300);
    </script>
<?php
    }
?>
    