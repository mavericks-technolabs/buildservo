<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class homepagecontroller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

       public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        /*$this->load->model('employee_model');
        $this->load->model('designation_model');
        $this->load->model('package_model');
        $this->load->model('service_model');*/
        
    }
    
    public function home_page() {
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('LoginController/index', 'refresh');
        } else 
        {
           //$id=$this->session->userdata('id');
           //$data['home_detail'] = $this->Home_model->get_home();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->model("Home_model");
            $result["ans"]=$this->Home_model->searchhomelist();
            // $data = $result["ans"];
            /*echo"<pre>";
            print_r($result);
             exit();*/
            $this->load->view('home/home-page',$result);
            $this->load->view('includes/footer');
        }
    }
     public function home_list() {
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('LoginController/index', 'refresh');
        } else 
        {
           //$data['home_detail'] = $this->Home_model->get_home();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('home/home-list');
            /*$result["ans"]=$this->Home_model->searchhomelist();*/
            $this->load->view('includes/footer');
        }
    }

    public function homelist_add()
    {

        $config=[
                    'upload_path'=>'./uploads/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->load->library('form_validation');

        /*$this->form_validation->set_rules('title',' title','required');
        $this->form_validation->set_rules('description', 'description','required');*/            
            
        //$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if($this->upload->do_upload('userfile'))
        {
            $title=$this->input->post("title");
            $description=$this->input->post("description");
            $data=$this->upload->data();
            $image_path=$data["raw_name"].$data['file_ext'];
            $newsdata = ['title'=>$title,'description'=>$description,'user_file'=>$image_path];

            $this->load->model('Home_model');
            if($this->Home_model->get_home($newsdata))
            {
                $error="Image Created Successfully.";
                $this->session->set_flashdata('error',$error);
                $this->session->set_flashdata('status','btn-success');
                redirect(base_url('home-page'));
            }
            else
            {
                redirect(base_url('home-list'));                              
            }
        }
        else
        {
           
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('home/home-list');
            $this->load->view('includes/footer');                             
        }
    }

    public function home_editlist() 
    {
        if (!$this->session->userdata('logged_in')) 
        {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('LoginController/index', 'refresh');
        } 
        else 
        {
            //$data['home_detail'] = $this->Home_model->get_home();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->model('Home_model');
            $result['ans'] = $this->Home_model->find_homeedit();
           /* echo"<pre>";
            print_r($result);
            exit();*/
            $this->load->view('home/home-edit',$result);
            $this->load->view('includes/footer');
        }
    }
    public function home_update()
    {
       /*$config=[
                    'upload_path'=>'./uploads/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->load->library('form_validation');
        if($this->upload->do_upload('userfile'))
        {
            $title=$this->input->post("title");
            $description=$this->input->post("description");
            $data=$this->upload->data();
            $image_path=$data["raw_name"].$data['file_ext'];
                
            $newsdata = ['title'=>$title,'description'=>$description,'user_file'=>$image_path];

            $this->load->model('Home_model');
            if($this->Home_model->update_homelist($newsdata))
            {
                $error="Image Created Successfully.";
                $this->session->set_flashdata('error',$error);
                $this->session->set_flashdata('status','btn-success');
                redirect(base_url('home-page'));
            }
            else
            {
                redirect(base_url('home-list'));                              
            }
        }
        else
        {
           
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('home/home-list');
            $this->load->view('includes/footer');                             
        }
    }*//*$config=[
                            'upload_path'=>'./uploads/',
                            'allowed_types'=>'jpg|gif|png|jpeg',
                        ];

                $this->load->library('upload',$config);
                $this->load->library('form_validation');
                $this->form_validation->set_rules('title','title','required');
                $this->form_validation->set_rules('description','description','required');
                $this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

                if($this->upload->do_upload('userfile'))
                {
                    $title=$this->input->post("title");
                    $description=$this->input->post("description");
                    $image_old_path= "./".$this->input->post("image_old_path");
                    $data=$this->upload->data();
                    $image_path="uploads/".$data["raw_name"].$data['file_ext'];


                    $data=['user_file'=>$image_path,'title'=>$title,'description'=>$description];
                    $this->load->model('Home_model');

                    if($this->Home_model->edit_homelist())
                    {
                        $this->load->helper("file");
                        unlink($image_old_path);


                        $error="Edited Successfull";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        $this->load->view('home/home-edit');
                    }else
                    {
                        $error=" not Added";
                        $this->session->set_flashdata('error',$error);

                        $this->session->set_flashdata('status','btn-danger');
                        return redirect('home/home-edit');
                    }
                }
                else
                {
                    $upload_errors=$this->upload->display_errors();
                   
                }*/
        $get=$this->input->get();
        if (!$this->session->userdata('logged_in')) 
        {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('LoginController/index', 'refresh');
        } 
        else 
        {
            if($this->input->post())
            {
                $this->form_validation->set_rules('title','title','required');
                $this->form_validation->set_rules('description','description','required');
                /* $this->form_validation->set_error_delimiters('<div class="error">', '</div>');*/
                 $post = $this->input->post();
                if($this->form_validation->run() == TRUE)
                {
                    if (!empty($_FILES['userfile']['name'])) 
                    {
                       $config=
                       [
                        'upload_path'=>'./uploads/',
                        'allowed_types' => 'gif|jpg|png',
                       ];
                        $this->load->library('upload', $config);
                        $this->upload->do_upload('userfile');
                        $upload_data = $this->upload->data();
                        $user_file= $upload_data['file_name'];   
                    } 
                    else 
                    {
                        $userfile= !empty($post['userfile_hidden'])?$post['userfile_hidden']:'';   
                    }
                    $data=$post;
                    if(isset($details['userfile_hidden']))
                    {
                        unset($details['userfile_hidden']);
                    }
                    $data['userfile'] = $userfile;
                    $result = $this->Home_model->homepageeditprofile($homepage_id,$data);
                    if($result) 
                    {
                        $this->session->set_flashdata('add_success', 'Feedback Services Updated Succesfully');
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        $this->load->view('home/home-edit');
                    }
                }
                        else 
                        {
                        $this->session->set_flashdata('add_failed', 'Failed to update feedbck services');
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-danger');
                        return redirect('home/home-edit');
                        }
            }
        }
}

    public function delete_homeaddpage($homepage_id)
    {
       $this->load->model("Home_model");
        if($this->Home_model->deletehomelist($homepage_id)) 
          {
             $this->session->set_flashdata('Delete',"Deleted Successfully");
             return redirect('home-page');
          }
        else
          {    
             $this->session->set_flashdata('error',"Failed to delete, please try again");
             return redirect('home-page');
          }
    }// public function delete_job($job_id)

}