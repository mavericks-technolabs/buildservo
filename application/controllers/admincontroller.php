<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class admincontroller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('customer_model');
        $this->load->model('employee_model');
        $this->load->model('service_model');
        $this->load->model('designation_model');
        $this->load->model('admin_model');
        
    }
    
//    public function index() {
//            
//        if (!$this->session->userdata('logged_in')) {
//            $this->session->set_flashdata('access_denied', 'Please login');
//            redirect('LoginController/index', 'refresh');
//        } else {
//           
//            $data['service_list'] = $this->service_model->get_service();
//            $this->load->view('includes/header');
//            $this->load->view('includes/sidebar');
//            $this->load->view('service/list', $data);
//            $this->load->view('includes/footer');
//        }
//    }
    
    public function customers_selected_services(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $data['selected_service_list'] = $this->service_model->get_selected_service();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('admin/selected_service_list', $data);
            $this->load->view('includes/footer');
        }
    }
    public function customers_cancelled_services(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $data['selected_service_list'] = $this->service_model->get_cancelled_service();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('admin/cancelled_service_list', $data);
            $this->load->view('includes/footer');
        }
    }
    public function customer_selected_services_view() {
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $get=$this->input->get();
            if($this->input->post()){
                $details = $this->input->post();

                $details['updated_at'] = date('Y-m-d H:i:s');
                $result = $this->service_model->update_selected_service_status($details);
                if ($result) {
                    $employee=$this->service_model->get_employee_details($details['employee_id']);
                    $customer=$this->service_model->get_customer_details($details['customer_id']);
                    $service=$this->service_model->get_service_name($details['service_id']);
                    $booking=$this->service_model->get_service_details($details['id']);
                    $date=date('Y-m-d');
                    $msg="";
                    $start_date=date('Y-m-d', strtotime($date. ' + 2 days'));
                    /*Pending=1,Confirmed=2,Cancelled=3,In progress=4,Completed=5*/
                    if ($booking->service_status==1) {
                        $msg="Dear,".$customer->customer_first_name.",your booking is Pending.\r\n Emplyee Assigned:".$employee->name.".";
                    }
                    elseif ($booking->service_status==2) {
                        $msg="Hi,".$customer->customer_first_name.",our request has been confirmed for the SeviceElectical Installation & Maintanace Worker assigned for the service is".$service."\r\n Worker assigned for the service is:".$employee->name.".\r\n Thank You, \r\n Buildservo";   
                    }
                    elseif ($booking->service_status==3) {
                        $msg="Hi,".$customer->customer_first_name.",as discussed, your request has been cancelled for the service".$service."due to quote is not in budget/ Location is out of service/other\r\n Thank You, \r\n Buildservo";
                    }
                    elseif ($booking->service_status==4) {
                        $msg="Hi,".$customer->customer_first_name.",the work for the service".$service."will started by".$start_date."with an assigned employee".$employee->name.".\r\n If not started on mentioned date & time, please contact us on 9067-9067-20. \r\nThank You, \r\nBuildservo";
                    }
                    elseif ($booking->service_status==5) {
                        $msg="Hi,".$customer->customer_first_name.",your Service has been completed Succesfully.\r\n Emplyee Assigned:".$employee->name.".";
                    }

                    /*Message Code*/
                        $username = "p.ahire04@gmail.com";
                        $hash = "1b0549f5fed8db23be5119ad8c4f651b2b49fb3296af9496ab742160982a9e30";

                        // Config variables. Consult http://api.textlocal.in/docs for more info.
                        $test = "0";

                        // Data for text message. This is the text message data.
                        $sender = "TXTLCL"; // This is who the message appears to be from.
                        $numbers =/* "9503641654"*/$customer->customer_mob; // A single number or a comma-seperated list of numbers
                        $message = $msg;
                        // 612 chars or less
                        // A single number or a comma-seperated list of numbers
                        $message = urlencode($message);
                        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
                        $ch = curl_init('http://api.textlocal.in/send/?');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $result = curl_exec($ch); // This is the result from the API
                        curl_close($ch);
                        /*Message Code*/



                    $this->session->set_flashdata('add_success', 'Selected Service updated Succesfully');
                    return redirect('admin/customers_selected_services', 'refresh');
                } else {
                    $this->session->set_flashdata('add_failed', 'Failed to update');
                    $selected_service_details = $this->service_model->get_selected_service_by_id($details['id']);
                    $data['selected_service_detail'] = $selected_service_details;
                    $data['service_detail']=$this->service_model->get_service_by_id($selected_service_details['service_id']);    
                    $data['employee_list']=$this->employee_model->get_employee();    
                    $this->load->view('includes/header');
                    $this->load->view('includes/sidebar');
                    $this->load->view('admin/selected_service_view', $data);
                    $this->load->view('includes/footer');
                }
            }else{
                $selected_service_details = $this->service_model->get_selected_service_by_id($get['id']);
                $data['selected_service_detail'] = $selected_service_details;
                $data['service_detail']=$this->service_model->get_service_by_id($selected_service_details['service_id']);    
                $data['employee_list']=$this->employee_model->get_employee();    
                $this->load->view('includes/header');
                $this->load->view('includes/sidebar');
                $this->load->view('admin/selected_service_view', $data);
                $this->load->view('includes/footer');
            }
        }
    }
    public function customer_cancelled_services_view() {
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $get=$this->input->get();
            
                $selected_service_details = $this->service_model->get_selected_service_by_id($get['id']);
                $data['selected_service_detail'] = $selected_service_details;
                $data['service_detail']=$this->service_model->get_service_by_id($selected_service_details['service_id']);    
                $data['employee_list']=$this->employee_model->get_employee();    
                $this->load->view('includes/header');
                $this->load->view('includes/sidebar');
                $this->load->view('admin/cancelled_service_view', $data);
                $this->load->view('includes/footer');
            
        }
    }
    public function contact_us(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $data['contact_list'] = $this->admin_model->get_contact();
            //pr($data);die;
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('admin/list_contact_us', $data);
            $this->load->view('includes/footer');
        }
    }
    public function contact_delete()
    {   
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('LoginController/index', 'refresh');
        } else {
            $get=$this->input->get();
            if(!empty($get)){
                $result=$this->admin_model->delete_contact($get['id']);
                if($result){
                    $this->session->set_flashdata('add_success', 'Contact  Deleted Succesfully');
                    return redirect('admin/contact_us', 'refresh');
                }else{
                    $this->session->set_flashdata('add_failed', 'Contact cannot deleted');
                    return redirect('admin/contact_us', 'refresh');
                }
            }else{
                return redirect('admin/contact_us', 'refresh');
            }
        }
        
    }
    public function feedback(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $data['feedback_list'] = $this->admin_model->get_feedback();
           /* echo "<pre>";
            print_r($data);
            exit();*/
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('admin/list_feedback', $data);
            $this->load->view('includes/footer');
        }
    }

    public function globalsettings(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $data['globalsettings'] = $this->admin_model->get_globalsettings();
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar');
            $this->load->view('global_settings', $data);
            $this->load->view('includes/footer');
        }
    }
    public function addglobalsettings(){
            
        if (!$this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('admin', 'refresh');
        } else {
            $settings=$this->input->post();
            $result=$this->admin_model->updatesettings($settings);
            if ($result) 
            {
                $this->session->set_flashdata('update_success', 'Settings Updated Successfully');
            }
            else
            {
                $this->session->set_flashdata('update_failed', 'Something went wrong.');
            }
            redirect('admin/globalsettings','refresh');
        }
    }
}
