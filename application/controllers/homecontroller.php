<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class homecontroller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('customer_model');
        $this->load->model('employee_model');
        $this->load->model('designation_model');
        $this->load->model('login_model');
        $this->load->model('service_model');
        $this->load->model('package_model');
        $this->load->model('admin_model');
        $this->load->model('blog_model');
    }

    public function login() {
        if ($this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('dashboard', 'refresh');
        }else{
            $data['globalsettings']=$this->admin_model->get_globalsettings();
            if($this->input->post()){
                $this->form_validation->set_rules('customer_email','E-mail', 'required');
                $this->form_validation->set_rules('customer_password', 'Password', 'trim|required');
                if($this->form_validation->run() == TRUE){
                    $details = $this->input->post();
                    $result=$this->login_model->login_validate($details);
                    if($result){
                        $session_data=array('customer_profile_id'=>$result['customer_profile_id'],
                                            'customer_name'=>$result['customer_first_name'],
                                            'customer_mob'=>$result['customer_mob'],
                                            'customer_email'=>$result['customer_email'],
                                            'logged_in' => TRUE,
                                            'type'=>2,
                                        );
                        $this->session->set_userdata($session_data);
                        $this ->session-> set_flashdata('Message','Successfully Login'); 
                        redirect('dashboard', 'refresh');

                    }else{
                        $this ->session-> set_flashdata('Error','Email or Password is incorrect'); 
                        $this->load->view('frontend/includes/header');
                        $this->load->view('frontend/login');
                        $this->load->view('frontend/includes/footer',$data);
                    }
                }else{
                    $this->load->view('frontend/includes/header');
                    $this->load->view('frontend/login');
                    $this->load->view('frontend/includes/footer',$data);
                }
            }else{
                $this->load->view('frontend/includes/header');
                $this->load->view('frontend/login');
                $this->load->view('frontend/includes/footer',$data);
            }
        }
    }

    public function register() {

        if ($this->session->userdata('logged_in')) {
            $this->session->set_flashdata('access_denied', 'Please login');
            redirect('dashboard', 'refresh');
        }else{
            $data['globalsettings']=$this->admin_model->get_globalsettings();
            if($this->input->post()){
                $this->form_validation->set_rules('customer_first_name', 'First Name', 'trim|required');
                $this->form_validation->set_rules('customer_last_name', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('customer_mob', 'Mobile Number', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]');
                $this->form_validation->set_rules('customer_email','E-mail', 'required');
                $this->form_validation->set_rules('customer_address', 'Address', 'trim|required');
                $this->form_validation->set_rules('customer_city', 'City', 'trim|required');
                $this->form_validation->set_rules('customer_pincode', 'Pincode', 'trim|required|regex_match[/^[0-9]{6}$/]');
                $this->form_validation->set_rules('customer_password', 'Password', 'trim|required');
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[customer_password]');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                if($this->form_validation->run() == TRUE){
                    $details = $this->input->post();
                    $mobile=$this->input->post('customer_mob');
                    $mobile_available=$this->customer_model->check_if_mobile_exists($mobile);
                    $email=$this->input->post('customer_email');
                    /* $customer_password=md5($this->input->post('customer_password'));*/
                    $email_available=$this->customer_model->check_if_email_exists($email);
                    if ($mobile_available) 
                    {
                        $this->session->set_flashdata('mobile_failed', 'This mobile is already registered with us.');
                        return redirect('register', 'refresh');
                    }
                    elseif ($email_available) 
                    {
                        $this->session->set_flashdata('email_failed', 'This email is already registered with us.');
                        return redirect('register', 'refresh');
                    }
                    else
                    {
                        if(isset($details['confirm_password'])){
                        unset($details['confirm_password']);
                    }
                    $details['customer_password'] = $details['customer_password'];
                    $details['is_active'] = 1;
                    $details['is_deleted'] = 0;
                    $details['created_at'] = date('Y-m-d H:i:s');
                    $details['updated_at'] = date('Y-m-d H:i:s');
                    $result = $this->customer_model->add_customer($details);
                    if ($result) {
                        $mobile=$details['customer_mob'];
                        $name=$details['customer_first_name'];
                        $login=$details['customer_email'];
                        $password=md5($this->input->post('customer_password'));

                        $username = "p.ahire04@gmail.com";
                        $hash = "1b0549f5fed8db23be5119ad8c4f651b2b49fb3296af9496ab742160982a9e30";

                        // Config variables. Consult http://api.textlocal.in/docs for more info.
                        $test = "0";

                        // Data for text message. This is the text message data.
                        $sender = "TXTLCL"; // This is who the message appears to be from.
                        $numbers = $mobile; // A single number or a comma-seperated list of numbers
                        $message = "Dear,".$name." you have been successfully registered.\r\nYour Username :-".$login."\r\nYour Password :-".$password."\r\n www.buildservo.com";
                        // 612 chars or less
                        // A single number or a comma-seperated list of numbers
                        $message = urlencode($message);
                        $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
                        $ch = curl_init('http://api.textlocal.in/send/?');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $result = curl_exec($ch); // This is the result from the API
                        curl_close($ch);
                        //
                        $this->session->set_flashdata('add_success', 'Register Succesfully');
                        return redirect('login', 'refresh');
                    } else {
                        $this->session->set_flashdata('add_failed', 'Failed to add employee');
                        $data['title']='Add';
                        $this->load->view('frontend/includes/header');
                        $this->load->view('frontend/register');
                        $this->load->view('frontend/includes/footer',$data);
                    }
                    }
                    
                }else{
                    $data['title']='Add';
                    $this->load->view('frontend/includes/header');
                    $this->load->view('frontend/register');
                    $this->load->view('frontend/includes/footer',$data);
                }
            }else{

                $data['title']='Add';
                $this->load->view('frontend/includes/header');
                $this->load->view('frontend/register');
                $this->load->view('frontend/includes/footer',$data);
            }
        }
        
    }
    public function home() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['service_list']=$this->service_model->get_service();
        $data['feedback_list']=$this->admin_model->get_feedback();
        $data['employee_list']=$this->employee_model->get_employee();
        $data['customer_list']=$this->customer_model->get_customer();
        $this->load->view('frontend/includes/header');
        $this->load->model("Build_model");
        $data["service_list"]=$this->Build_model->homespecific();
        $this->load->view('frontend/home',$data);
        $this->load->view('frontend/includes/footer',$data);
    }

    public function about_us() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/about-us');
        $this->load->view('frontend/includes/footer',$data);
    }

    public function services() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['service_list']=$this->service_model->get_service();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/services',$data);
        $this->load->view('frontend/includes/footer',$data);
    }

    public function service_details() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $get=$this->input->get();
        $data['service_detail']=$this->service_model->get_service_by_id($get['id']);
        $data['service_list']=$this->service_model->get_service();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/service-details',$data);
        $this->load->view('frontend/includes/footer',$data);
    }

    public function pricing() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['package_list']=$this->package_model->get_package();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/pricing',$data);
        $this->load->view('frontend/includes/footer',$data);
    }

    public function blogs() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['blog_details']= $this->blog_model->get_blog();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/blogs',$data);
        $this->load->view('frontend/includes/footer',$data);
    }

    public function blog_details() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $get=$this->input->get();
        if(!empty($get)){
            $data['blog_detail']=$this->blog_model->get_blog_by_id($get['id']);
            $this->load->view('frontend/includes/header');
            $this->load->view('frontend/blog-details',$data);
            $this->load->view('frontend/includes/footer',$data);
        }else{
            redirect('blogs');
        }
    }

    public function contact_us() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        if($this->input->post()){
            $this->form_validation->set_rules('name', 'Your Name', 'trim|required');
            $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required|numeric|regex_match[/^[0-9]{10}$/]');
            $this->form_validation->set_rules('email_id','E-mail', 'required');
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            $this->form_validation->set_rules('business_hours', 'business_hours', 'trim|required');
            $this->form_validation->set_rules('business_saturday', 'business_saturday', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE){
                $details = $this->input->post();
                $details['created_at'] = date('Y-m-d H:i:s');
                $result = $this->login_model->add_contact($details);
                if ($result){
                    $this->session->set_flashdata('Message', 'Thank you for reaching us.Our executive will call you shortly regarding your enquiry');
                    return redirect('contact-us', 'refresh');
                } else {
                    $this->session->set_flashdata('Error', 'Failed to sent feedback');
                    $this->load->view('frontend/includes/header');
                    $this->load->view('frontend/contact-us');
                    $this->load->view('frontend/includes/footer',$data);
                }
            }else{
                $this->load->view('frontend/includes/header');
                $this->load->view('frontend/contact-us');
                $this->load->view('frontend/includes/footer',$data);
            }
        }else{
            $this->load->view('frontend/includes/header');
            $this->load->view('frontend/contact-us',$data);
            $this->load->view('frontend/includes/footer',$data);
        }
        
    }

    public function sitemap() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/sitemap');
        $this->load->view('frontend/includes/footer',$data);
    }

    public function faq() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/faq');
        $this->load->view('frontend/includes/footer',$data);
    }
    public function forgotpassword() {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['service_list']=$this->service_model->get_service();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/forgotpassword');
        $this->load->view('frontend/includes/footer',$data);
    }
    public function recoverpassword()
    {
        $mobile=$this->input->post('mobile');

        $mobile_available=$this->admin_model->check_if_mobile_exists($mobile);
        $details=$this->admin_model->getdetails($mobile);
          if ($mobile_available) 
          {
            $username = "p.ahire04@gmail.com";
            $hash = "1b0549f5fed8db23be5119ad8c4f651b2b49fb3296af9496ab742160982a9e30";

            // Config variables. Consult http://api.textlocal.in/docs for more info.
            $test = "0";
 
            // Data for text message. This is the text message data.
            $sender = "TXTLCL"; // This is who the message appears to be from.
            $numbers = $mobile; // A single number or a comma-seperated list of numbers
            $message = "Dear,".$details->customer_first_name."\r\nYour Username :-".$details->customer_email."\r\nYour Password :-".$details->customer_password."\r\nThank you,\r\nBuildservo";
            // 612 chars or less
            // A single number or a comma-seperated list of numbers
            $message = urlencode($message);
            $data = "username=".$username."&hash=".$hash."&message=".$message."&sender=".$sender."&numbers=".$numbers."&test=".$test;
            $ch = curl_init('http://api.textlocal.in/send/?');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); // This is the result from the API
            curl_close($ch);
                          //
            $this->session->set_flashdata('success' ,"Password sent successfully to your mobile number.");
          }
          else
          {
            $this->session->set_flashdata('failed' ,"Mobile number you entered is not registered with us.");
          }
          redirect('forgotpassword','refresh');
    }
    public function joinus()
    {
        $data['globalsettings']=$this->admin_model->get_globalsettings();
        $data['service_list']=$this->service_model->get_service();
        $this->load->view('frontend/includes/header');
        $this->load->view('frontend/join-us',$data);
        $this->load->view('frontend/includes/footer',$data);
    }
    public function addjoinus()
    {
        $joinus = $this->input->post();
        $result=$this->service_model->addjoinus($joinus);

        if($result)
        {
            $this->session->set_flashdata('success' ,"Your request for joining BUILDSERVO has been submitted successfully, we will contact you shortly.");
        }
        else
        {
            $this->session->set_flashdata('failed' ,"Something went wrong.");
        }
        redirect('joinus','refresh');
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('home','refresh');
    }
}
