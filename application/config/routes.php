<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'homecontroller/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login']='homecontroller/login';
$route['forgotpassword']='homecontroller/forgotpassword';
$route['recoverpassword']='homecontroller/recoverpassword';
$route['logout']='homecontroller/logout';
$route['register']='homecontroller/register';
$route['home']='homecontroller/home';
$route['about-us']='homecontroller/about_us';
$route['services']='homecontroller/services';
$route['service-details']='homecontroller/service_details';
$route['pricing']='homecontroller/pricing';
$route['blogs']='homecontroller/blogs';
$route['blog-details']='homecontroller/blog_details';
$route['contact-us']='homecontroller/contact_us';
$route['sitemap']='homecontroller/sitemap';
$route['faq']='homecontroller/faq';
$route['joinus']='homecontroller/joinus';
$route['addjoinus']='homecontroller/addjoinus';

$route['admin']="logincontroller/index";
$route['admin/login']="logincontroller/login";
$route['admin/logout']="logincontroller/logout";
$route['dashboard']="dashboardcontroller/dashboard";
$route['admin/customers_selected_services']="admincontroller/customers_selected_services";
$route['admin/customers_cancelled_services']="admincontroller/customers_cancelled_services";
$route['admin/customer_selected_services_view']="admincontroller/customer_selected_services_view";
$route['admin/customer_cancelled_services_view']="admincontroller/customer_cancelled_services_view";
$route['admin/contact_us']="admincontroller/contact_us";
$route['admin/contact_delete']="admincontroller/contact_delete";
$route['admin/feedback']="admincontroller/feedback";
$route['admin/globalsettings']="admincontroller/globalsettings";
$route['admin/addglobalsettings']="admincontroller/addglobalsettings";

/*Package Routes*/
$route['package']="packagecontroller/index";
$route['package/add']="packagecontroller/add";
$route['package/edit']="packagecontroller/edit";
$route['package/delete']="packagecontroller/delete";

/*Employee Routes*/
$route['employee']="employeecontroller/index";
$route['employee/add']="employeecontroller/add";
$route['employee/edit']="employeecontroller/edit";
$route['employee/delete']="employeecontroller/delete";

/*Blog Routes*/
$route['blog']="blogcontroller/index";
$route['blog/add']="blogcontroller/add";
$route['blog/edit']="blogcontroller/edit";
$route['blog/delete']="blogcontroller/delete";

/*Designation Routes*/
$route['designation']="designationcontroller/index";
$route['designation/add']="designationcontroller/add";
$route['designation/edit']="designationcontroller/edit";
$route['designation/delete']="designationcontroller/delete";

/*Service Routes*/
$route['service']="servicecontroller/index";
$route['service/add']="servicecontroller/add";
$route['service/delete']="servicecontroller/delete";
$route['service/edit']="servicecontroller/edit";
$route['service/service_booking']="servicecontroller/service_booking";
$route['service/service_booking_view']="servicecontroller/service_booking_view";
$route['service/selected_services']="servicecontroller/selected_services";
$route['service/book_service']="servicecontroller/book_service";
$route['service/selected_services_view']="servicecontroller/selected_services_view";
$route['service/selected_services_cancle']="servicecontroller/selected_services_cancle";

/*Customers Routes*/
$route['customer']="customercontroller/index";
$route['customer/add']="customercontroller/add_customer";
$route['customer/delete']="customercontroller/delete";
$route['customer/editprofile']="customercontroller/editprofile";
$route['customer/resetpassword']="customercontroller/resetpassword";
$route['customer/feedback']="customercontroller/feedback";
$route['customer/add_feedback']="customercontroller/add_feedback";
$route['get-employee-selected-service'] = "employeecontroller/getEmployeeByService";


/*Customer Routes*/

/*Customer Group Routes*/
$route['customer-groups']="customercontroller/customer_groups";
$route['add-customer-group']="customercontroller/add_customer_group";
$route['delete-group']="customercontroller/delete_group";
/*Customer Group Routes*/

/*Brand Routes*/
$route['brands']="brandcontroller/brands";
$route['add-brand']="brandcontroller/add_brand";
$route['delete-brand']="brandcontroller/delete_brand";
/*Brand Routes*/

/*Supplier Routes*/
$route['suppliers']="suppliercontroller/suppliers";
$route['add-supplier']="suppliercontroller/add_supplier";
$route['delete-supplier']="suppliercontroller/delete_supplier";
$route['supplier']="suppliercontroller/supplier";
$route['update-supplier']="suppliercontroller/update_supplier";
/*Supplier Routes*/

/*Barcode Routes*/
$route['barcodes']="barcodecontroller/barcodes";
$route['add-barcode']="barcodecontroller/add_barcode";
$route['delete-barcode']="barcodecontroller/delete_barcode";
$route['print-barcode']="barcodecontroller/print_barcode";
/*Barcode Routes*/

/*Product Routes*/
$route['products']="productcontroller/products";
$route['add-product']="productcontroller/add_product";
$route['delete-product']="productcontroller/delete_product";
/*Product Routes*/

/*Purchase Order Routes*/
$route['purchase-orders']="purchaseordercontroller/purchase_orders";
$route['add-purchase-order']="purchaseordercontroller/add_purchase_order";
$route['add-purchase-order-details']='purchaseordercontroller/add_purchase_order_details';
$route['delete-purchase-order']="purchaseordercontroller/delete_purchase_order";
$route['print-purchase-order/(:any)']="purchaseordercontroller/print_purchase_order/$1";
/*Purchase Order Routes*/

/*Product Stock Routes*/
$route['product-stock']="productcontroller/product_stock";
$route['add-product-stock']="productcontroller/add_product_stock";
$route['add-product-stock-details']="productcontroller/add_product_stock_details";
$route['delete-product-stock']="productcontroller/delete_product_stock";
/*Product Stock Routes*/

/*Sales Order Routes*/
$route['sales-orders']="salesordercontroller/sales_orders";
$route['add-sales-order']="salesordercontroller/add_sales_order";
$route['add-sales-order-details']='salesordercontroller/add_sales_order_details';
$route['delete-sales-order']="salesordercontroller/delete_sales_order";
$route['print-sales-order/(:any)']="salesordercontroller/print_sales_order/$1";
$route['get-customer-details']="salesordercontroller/get_customer_details";
$route['get-product-details']="salesordercontroller/get_product_details";
/*Sales Order Routes*/


/*Join us*/
$route['admin/joinus']="employeecontroller/joinus";
$route['join/delete']="employeecontroller/joindelete";
/*Join us*/
/*homepage*/
$route['home-page']="homepagecontroller/home_page";
$route['home-list']="homepagecontroller/home_list";
$route['home-edit']="homepagecontroller/home_editlist";
$route['delete_homeaddpage']="homepagecontroller/delete_homeaddpage";

/*homepage*/