<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employee_model extends CI_Model {

    function get_employee() {
        $this->db->where('is_deleted',0);
        $query=$this->db->get('tbl_employee');
        /* print_r($this->db->last_query());
        exit();*/
        return $query->result_array();
    }
    function get_employee_by_id($id) {
        $this->db->where('id',$id);
        $query=$this->db->get('tbl_employee');
         /*print_r($this->db->last_query());
        exit();*/
        return $query->row_array();
    }
    function add_employee($data) {
        $this->db->trans_start();
        $this->db->insert('tbl_employee', $data);
        $this->db->trans_complete();
        return true;
    }
    function edit_employee($data) {
        $this->db->trans_start();
        $this->db->where('id',$data['id']);
        $this->db->update('tbl_employee', $data);
        $this->db->trans_complete();
        /*print_r($this->db->last_query());
        exit();*/
        return true;
    }
    function delete_employee($id){
        $data=array('is_deleted'=>1);
        $this->db->trans_start();
        $this->db->where('id',$id);
        $this->db->update('tbl_employee', $data);
        $this->db->trans_complete();
        return true;
        
    }
    function joinus() {
        $this->db->select('*');
        $this->db->from('tbl_join_us');
        $query=$this->db->get();
        return $query->result_array();
    }
    function delete_join($id){
        
        return $this->db->delete('tbl_join_us',['id'=>$id]);
        
    }
    function get_selected_employee_by_employee_id($employee_id) {
        $this->db->select('tbl_employee.*,tbl_service.name');
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee.employee_id');
        
        $this->db->where('tbl_employee.employee_id',$employee_id);
        $this->db->order_by("id", "desc");
       
        $query=$this->db->get();
        /* print_r($this->db->last_query());
        exit();*/
        return $query->result_array();
    }
     public function employeefindcontact($mobile_no)
    {
        $this->db->select('*');
        $this->db->from ("tbl_employee");
        $this->db->where('mobile_no', $mobile_no);
            $q=$this->db->get();
        return $q->result();
    }// public function findcontact($contact)

}
