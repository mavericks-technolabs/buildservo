<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_model extends CI_Model {

    function get_service() {
        $this->db->where('is_deleted',0);
        $query=$this->db->get('tbl_service');
        return $query->result();
    }
    function get_service_by_id($id) {
        $this->db->where('id',$id);
        $query=$this->db->get('tbl_service');
        return $query->row_array();
    }
    function add_service($data) {
        $this->db->trans_start();
        $this->db->insert('tbl_service', $data);
        $this->db->trans_complete();
        return true;
    }
    function edit_service($data) {
        $this->db->trans_start();
        $this->db->where('id',$data['id']);
        $this->db->update('tbl_service', $data);
        $this->db->trans_complete();
        return true;
    }
    function delete_service($id){
        $data=array('is_deleted'=>1);
        $this->db->trans_start();
        $this->db->where('id',$id);
        $this->db->update('tbl_service', $data);
        $this->db->trans_complete();
        return true;
        
    }
    function book_service($data) {
        $this->db->trans_start();
        $this->db->insert('tbl_selected_services', $data);
        $this->db->trans_complete();
        return true;
    }
    function get_selected_service() {
        
        $this->db->select('tbl_selected_services.*,tbl_customer_profile.*,tbl_service.name');
        $this->db->from('tbl_selected_services');
        $this->db->join('tbl_customer_profile','tbl_customer_profile.customer_profile_id=tbl_selected_services.customer_id');
        $this->db->join('tbl_service','tbl_service.id=tbl_selected_services.service_id');
        $this->db->where("tbl_selected_services.service_status <>", 3);
        $this->db->order_by("id", "desc");
        $query=$this->db->get();
        return $query->result_array();
    }
    function get_cancelled_service() {
        
        $this->db->select('tbl_selected_services.*,tbl_customer_profile.*,tbl_service.name');
        $this->db->from('tbl_selected_services');
        $this->db->join('tbl_customer_profile','tbl_customer_profile.customer_profile_id=tbl_selected_services.customer_id');
        $this->db->join('tbl_service','tbl_service.id=tbl_selected_services.service_id');
        $this->db->where("tbl_selected_services.service_status =", 3);
        $this->db->order_by("id", "desc");
        $query=$this->db->get();
        return $query->result_array();
    }
    function get_selected_service_by_customer_id($customer_id) {
        $this->db->select('tbl_selected_services.*,tbl_customer_profile.*,tbl_service.name');
        $this->db->from('tbl_selected_services');
        $this->db->join('tbl_customer_profile','tbl_customer_profile.customer_profile_id=tbl_selected_services.customer_id');
        $this->db->join('tbl_service','tbl_service.id=tbl_selected_services.service_id');
        $this->db->where('tbl_selected_services.customer_id',$customer_id);
        $this->db->order_by("id", "desc");
       /* $this->db->group_by('service_id');*/
        $query=$this->db->get();
       /* print_r($this->db->last_query());
        exit();*/
        return $query->result_array();
    }
    function get_selected_service_by_ids($id) {
        $this->db->where('id',$id);
        $query=$this->db->get('tbl_selected_services');
        return $query->row_array();
    }
    function get_selected_service_by_id($id) {
        $this->db->select('tbl_selected_services.*,tbl_customer_profile.*,tbl_service.name');
        $this->db->from('tbl_selected_services');
        $this->db->join('tbl_customer_profile','tbl_customer_profile.customer_profile_id=tbl_selected_services.customer_id');
        $this->db->join('tbl_service','tbl_service.id=tbl_selected_services.service_id');
        $this->db->where('tbl_selected_services.id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }
    function getEmployeeByServiceId($id) {
        $this->db->select('te.*');
        $this->db->from('tbl_selected_services tss');
        $this->db->join('tbl_employee te','te.id = tss.employee_id');
        $this->db->where('tss.service_id',$id);
        $this->db->group_by('tss.employee_id');
        $query=$this->db->get();
        return $query->result_array();
    }
    function update_selected_service_status($data) {
        $this->db->trans_start();
        $this->db->where('id',$data['id']);
        $this->db->update('tbl_selected_services', $data);
        $this->db->trans_complete();
        return true;
    }


    /*Message Code*/
    function get_customer_name($id) {
        $this->db->select('customer_first_name');
        $this->db->from('tbl_customer_profile');
        $this->db->where('customer_profile_id',$id);
        $query=$this->db->get();
        return $query->row('customer_first_name');
    }
    function get_customer_mobile($id) {
        $this->db->select('customer_mob');
        $this->db->from('tbl_customer_profile');
        $this->db->where('customer_profile_id',$id);
        $query=$this->db->get();
        return $query->row('customer_mob');
    }
    function get_service_name($id) {
        $this->db->select('name');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row('name');
    }
    function get_service_details($id) {
        $this->db->select('*');
        $this->db->from('tbl_selected_services');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row();
    }
    function get_employee_details($id) {
        $this->db->select('*');
        $this->db->from('tbl_employee');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row();
    }
    function get_customer_details($id) {
        $this->db->select('*');
        $this->db->from('tbl_customer_profile');
        $this->db->where('customer_profile_id',$id);
        $query=$this->db->get();
        return $query->row();
    }
    /*function get_service_name($id) {
        $this->db->select('*');
        $this->db->from('tbl_service');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row();
    }*/
    function addjoinus($data) {
        $this->db->trans_start();
        $this->db->insert('tbl_join_us', $data);
        $this->db->trans_complete();
        return true;
    }
}
